# Welcome to L02_C20

Hi! This is the official repository for the Controller/Sensors team for Lift Team 2.


## Repository Structure
```
+-- _config.yml
+-- ElevatorSystem
|   +-- Controller_Sensor
|	|	+-- Elevator.cpp
|	|	+-- Elevator.h
|	|	+-- keywords.txt
|	|	+-- Motor.cpp
|	|	+-- Motor.h
|	|	+-- SensorHallEffect.cpp
|	|	+-- SensorHallEffect.h
|   +-- ElevatorSystem.ino
+-- README.md
```

## How to use?

Simply move the **Controller_Sensor** directory into your sketchbook directory and open **ElevatorSystem.ino**.

## Where is my Sketchbook directory?
**On Linux machines**, the folder is named "Sketchbook" and it is typically located in /home/  
  
**On Windows and Macintosh machines**, the default name of the folder is "Arduino" and is located in your Documents folder. E.g. Documents\Arduino\libraries\myLibraryFolder