#pragma once
#include <Arduino.h>
#include <Elevator.h>
#include <Motor.h>
#include <SensorHallEffect.h>

// Elevator Singleton Setup

Elevator* Elevator::elevatorInstance = NULL;

Elevator* Elevator::getInstance() {
    if(!elevatorInstance)
        elevatorInstance = new Elevator;

    return elevatorInstance;
}

bool hardwareConnected = false;
int progress = 0; // Used to emulate hardware when no hardware is connected.
int speed = 25; // Set speed at which elevator will move.

/**
 *  Elevator Intialisation:
 *      - Level is set
 *      - Status is set
 *      - Motor and Sensor Intialisation
 *      - Elevator is reset to initial level if not set
 **/
void Elevator::setup(bool isHardwareConnected) {

    // Setup hardware if connected
    if(isHardwareConnected) {

        hardwareConnected = true;

        int motorPin = 23;
        int doorPin = 24;
        int sensorLevel1 = 35;
        int sensorLevel2 = 36;
        int sensorLevel3 = 37;
        int sensorLevel4 = 38;
        int sensorLevel5 = 39;

        Serial.print("Initialising Pins...");
        _motor.setup(motorPin);
        _door.setup(doorPin);
        _sensorLevel1.setup(sensorLevel1);
        _sensorLevel2.setup(sensorLevel2);
        _sensorLevel3.setup(sensorLevel3);
        _sensorLevel4.setup(sensorLevel4);
        _sensorLevel5.setup(sensorLevel5);
        Serial.println("OK");

        sensor[0] = _sensorLevel1;
        sensor[1] = _sensorLevel2;
        sensor[2] = _sensorLevel3;
        sensor[3] = _sensorLevel4;
        sensor[4] = _sensorLevel5;

    }

    // Intialise Variables for Elevator
    // TODO 1 Move this to resetElevator() method and let State Machine team know to reset the elevator first before starting.
    while(!sensor[0].isActivated()) {
        _motor.speedAdjust(90 + speed);
    }
    _motor.stop();
    _status = 0; // Elevator is ready for operation
    _level = 1; // Assume elevator level 1

}



int Elevator::moveToLevel(int level) {

    // Print Status Codes
    if(_status != 0) {

        Serial.println("Elevator Message: The machine is currently busy at the moment.");

    } else if(_status == 0) {

        Serial.println("Elevator Message: Preparing to move to another level.");
        Serial.println("Warning Message: Elevator will not respond to any requests until target level has been reached.");

        _status = -1;
        _targetLevel = level;

    }

    // Code that controls Motors and Sensor goes here
    if(hardwareConnected) {

        if(sensor[level - 1].isActivated()) {

            _status = 0;
            setLevel(level);
            _motor.stop();
            return 0;

        } else {

            if(_targetLevel > _level) {

                _motor.speedAdjust(90 - (speed / 2));

            } else {

                _motor.speedAdjust(90 + speed);

            }

            delay(10);

        }

    } 
    
    // Run code to test State Machine's Code in the event the Motors and Sensor are not working.
    else {
        
        if(progress >= 20) {
            progress = 0;
            this->_status = 0;
            this->_level = level;
            Serial.print("Elevator Message: The machine has reached level ");
            Serial.println(this->_level);
            return _status;
        } else {
            progress++;
            return this->_status;
        }

    }

}

/**
 *  Completely Stop the elevator 
 **/
void Elevator::emergencyStop() {

    Serial.println("Warning Message: Emergency Button has been pressed!");

    if(hardwareConnected) {
        _motor.stop();
    }

}

/**
 * Reset Elevator to ground level on startup
 **/
void Elevator::resetElevator() {

    // Move elevator down
    if(sensor[0].isActivated())
        _motor.stop();

    // When lowest level's sensor is detected, stop!
    _motor.speedAdjust(90 + speed);

}

void Elevator::setLevel(int level) {

    _level = level;

}

int Elevator::getlevel() {

    return _level;

}

int count = 0;

int Elevator::openDoor() {

    _status = 2; // Elevator door is open

    // Code to open door
    _door.speedAdjust(-360);
    count++;

    // After some amount of time return OK
    if(count > 1000) {

        count = 0;
        _door.stop();
        return 0;

    }

    return _status

}

int Elevator:: closeDoor() {

    _door.speedAdjust(360);
    count++;

    // After some amount of time return OK
    if(count > 1000) {

        _status = 0; // Elevator is Ready to Move
        count = 0;
        _door.stop();
        return 0;

    }

   return _status;

}
