#pragma once
#include <Arduino.h>

class SensorHallEffect {
	public:
        void setup(int pin);
        bool isActivated();

	private:
        int _pin;
};
