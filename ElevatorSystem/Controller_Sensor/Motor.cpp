#include <Arduino.h>
#include "Motor.h"

/**
 * Motor pin and Servo initialization
 */
void Motor::setup(int motorPin) {
	_pin = motorPin;
	_servo.attach(motorPin);
	speedAdjust(90);
}

/**
 * The following function will adjust the selected motor's
 * speed according to the following:
 *
 * 0 - FULL SPEED in one direction
 * 90 - STOPPED
 * 180 - FULL Speed in the other direction
 */
void Motor::speedAdjust(int targetSpeed) {

	if(_speed != targetSpeed) {
		_speed = targetSpeed;
		_servo.write(targetSpeed);
	}
}

int motorReverse = 0, motorStop = 90, motorForward = 180;
int currentTime = 0, previousTime = 0, elapsedTime = millis();
int rampStep = 200;

/**
 *  Elevator Carriage Controls 
 **/
void Motor::rampControl(bool accelerate, bool up) {

	speedAdjust(_speed);

	if (up) {
		if (accelerate) {
			if (_speed < motorForward) {

				if (millis() - previousTime > rampStep) {
				speedAdjust(_speed + 3);
				previousTime = millis();
				}

			}
		} else {
			if(_speed > motorStop) {

				if(millis() - previousTime > rampStep) {
					speedAdjust(_speed - 3);
					previousTime = millis();
				}

			}
		}
  	} else {
		if (accelerate) {

			while (_speed > motorReverse) {

				if (millis() - previousTime > rampStep) {
					speedAdjust(_speed - 3);
					previousTime = millis();
				}

			}
		
		} else {

			if (_speed < motorStop) {

				if (millis() - previousTime > rampStep) {
					speedAdjust(_speed + 3);
					previousTime = millis();
				}

			}

		}
  	}
    
}

/**
 * Halt motor movement
 */
void Motor::stop() {

	speedAdjust(90);

}
