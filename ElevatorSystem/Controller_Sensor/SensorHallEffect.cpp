#include <Arduino.h>
#include <SensorHallEffect.h>

void SensorHallEffect::setup(int pin) {

    _pin = pin;
    pinMode(pin, INPUT);

}

bool SensorHallEffect::isActivated() {

    if(digitalRead(_pin) == LOW) {
        return true;
    }

    return false;

}