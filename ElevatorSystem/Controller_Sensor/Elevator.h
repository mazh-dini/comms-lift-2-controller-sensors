#pragma once
#include <Arduino.h>
#include "Motor.h"
#include "SensorHallEffect.h"

class Elevator {

        private:

                /**
                 *  The elevator states are as follows:
                 *      -1 = Busy
                 *       0 = Standby / Job Complete
                 *       1 = Emergency
                 *       2 = Door is open
                 **/
                int _status;

                /**
                 *  The elevator can have as many levels as one desires,
                 *  this is dependent on the state machine subsystem as they control
                 *  what level one can / cannot travel to. 
                 **/
                int _level;
                int _targetLevel;

                /**
                 *  This class is a singleton, only one instance of it
                 *  can be declared at any time. This allows us to access
                 *  this class from any file by including this header file
                 *  and accessing its functions using the following snippet:
                 *      Elevator::getInstance()->functionYouWantToUse();
                 **/
                static Elevator* elevatorInstance;
                
                /**
                 *  Hardware to be controlled by the elevator's functions.
                 **/
                Motor _motor;
                Motor _door;
                SensorHallEffect sensor[5];
                SensorHallEffect _sensorLevel1;
                SensorHallEffect _sensorLevel2;
                SensorHallEffect _sensorLevel3;
                SensorHallEffect _sensorLevel4;
                SensorHallEffect _sensorLevel5;

                /**
                 *  This class keeps track of the elevator's current level,
                 *  to keep a reliable recording of the level this is set as
                 *  private.
                 **/
                void setLevel(int level);

        public:

                void setup(bool isHardwareConnected);
                void resetElevator();
                void emergencyStop();
                int getlevel();
                int moveToLevel(int level);
                int openDoor();
                int closeDoor();

                static Elevator* getInstance();

};
