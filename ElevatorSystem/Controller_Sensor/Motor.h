#pragma once
#include <Arduino.h>
#include <Servo.h>

class Motor {
	private:
	  int _pin;
		int _speed;
	  Servo _servo;

	public:
	  void setup(int motorPin);
	  void speedAdjust(int targetSpeed);
	  void rampControl(bool accelerate, bool up);
	  void stop();
};